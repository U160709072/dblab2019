select country , count(CustomerID)
from Customers
group by Country;

select ShipperName, count(Orders.OrderID) as numberOfOrders
from Shippers join Orders on Shippers.ShipperId = Orders.ShipperID
group by Shippers.ShipperID
order by numberOfOrders asc;